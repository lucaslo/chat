import {
  SEND_MESSAGE,
  RECEIVE_MESSAGE
} from "./actions"

import {
  DISCONNECT_USER,
  CONNECT_USER
} from "../user/actions"

export function message (state = [], action) {
  let newMessage
  switch (action.type) {
    case SEND_MESSAGE:
      return [...state]
    case RECEIVE_MESSAGE:
      newMessage = action.message
      delete newMessage.type
      return [...state, newMessage]
    case DISCONNECT_USER:
    case CONNECT_USER:
      newMessage = {from: action.message.from, text: action.message.text}
      return [...state, newMessage]
    default:
      return state
  }
}
