import { makeActionCreator } from "../util"

export const SEND_MESSAGE = "SEND_MESSAGE"
export const sendMessage = makeActionCreator(SEND_MESSAGE, "message")

export const RECEIVE_MESSAGE = "RECEIVE_MESSAGE"
export const receiveMessage = makeActionCreator(RECEIVE_MESSAGE, "message")
