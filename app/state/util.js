export function makeActionCreator (type, ...argNames) {
  return function (...args) {
    let action = { type }
    argNames.forEach((arg, index) => {
      action[argNames[index]] = args[index]
    })
    return action
  }
}

export function asyncCreator (api, actionName, resourceName) {
  let consts = ["REQUEST", "SUCCESS", "FAILURE"].map((el) => actionName + "_" + el)
  let request = makeActionCreator(consts[0], "parameters")
  let success = makeActionCreator(consts[1], resourceName)
  let failure = makeActionCreator(consts[2], "error")

  let action = function (...parameters) {
    return function (dispatch) {
      dispatch(request(...parameters))

      return api(...parameters)
        .then(function (response) {
          dispatch(success(response.data))
        })
        .catch(function (error) {
          dispatch(failure(error))
          throw error
        })
    }
  }

  return {
    consts: {
      REQUEST: consts[0],
      SUCCESS: consts[1],
      FAILURE: consts[2]
    },
    request,
    success,
    failure,
    action
  }
}
