import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  DISCONNECT_USER,
  CONNECT_USER
} from "./actions"


export function user (state = {logged: false, waiting: false, id: null, nickname: "", onlineUsers: []}, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {...state, waiting: true}
    case LOGIN_SUCCESS:
      return {...state, waiting: false, id: action.message.id, logged: true, nickname: action.message.nickname}
    case CONNECT_USER:
    case DISCONNECT_USER:
      return {...state, waiting: false, onlineUsers: action.message.users}
    default:
      return state
  }
}
