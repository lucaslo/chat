import { makeActionCreator } from "../util"

export const LOGIN_REQUEST = "LOGIN_REQUEST"
export const loginRequest = makeActionCreator(LOGIN_REQUEST, "info")

export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const loginSuccess = makeActionCreator(LOGIN_SUCCESS, "message")

export const DISCONNECT_USER = "DISCONNECT_USER"
export const disconnectUser = makeActionCreator(DISCONNECT_USER, "message")

export const CONNECT_USER = "CONNECT_USER"
export const connectUser = makeActionCreator(CONNECT_USER, "message")
