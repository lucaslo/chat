import io from "socket.io-client"
import {store} from './main.js'
import {loginSuccess, disconnectUser,connectUser} from './state/user/actions'
import {receiveMessage} from './state/message/actions'

let socket = null

export function initSocket (nick) {

  socket = io("localhost:3003", {
    forceNew: true,
    transports: ["websocket"],
    upgrade: false,
  })
  socket.emit("login", nick)
  socket.on("systemMessage", (message)=>{
    console.log("The system has send you a message")
    console.log(message)
    if (message.type === "LOGIN_SUCCESS") {
      store.dispatch(loginSuccess(message))
    }
    if (message.type === "NEW_MESSAGE") {
      store.dispatch(receiveMessage(message))
    }
    if (message.type === "DISCONNECT_USER") {
      store.dispatch(disconnectUser(message))
    }
    if (message.type === "CONNECT_USER") {
      store.dispatch(connectUser(message))
    }
  })

}

export function getSocket(){
  return socket
}
