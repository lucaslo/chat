<router class="router">
  <login if="{!this.state.user.logged}"></login>
  <chat if="{this.state.user.logged}"></chat>

  <script>
    import './logs.tag'
    import './login.tag'
    import './chat.tag'
    import {store} from './main.js'
    import {loginRequest} from './state/user/actions'
    this.number = null
    this.logs = []

    store.subscribe(() =>{
      this.state = store.getState()
    })

    store.dispatch(loginRequest("ola"))

  </script>
  <style>
    router{
      display: grid;
    }
  </style>
</router>
