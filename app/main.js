import riot from 'riot'
import { createStore, combineReducers } from 'redux'
import 'riot-hot-reload'
import './router.tag'

import {user} from "./state/user/reducer.js"
import {message} from "./state/message/reducer.js"

export const store = createStore(combineReducers({user,message}))


riot.mount('router', {
  title: 'Hi there!'
})

store.subscribe(() =>{
  riot.update()
})
