<login>
  <div class="loginHeader">
    <i class="fa fa-comment"></i>
    <h2> WebpackChat </h4>
    <small> by Lucas Lima </small>
  </div>
  <form class="loginForm animated fadeIn" onsubmit="{loginSubmit}">
    <input class="inputLogin" type="text" name="nick" placeholder="Nickname">
    <input type="submit" value="Start">
  </form>

  <script>
    import {store} from './main.js'
    import {loginRequest} from './state/user/actions'
    import {initSocket} from './socketConnector'
    store.subscribe(() =>{
      this.state = store.getState()
      this.update();
    })
    this.loginSubmit = (evento) => {
      event.preventDefault()
      const credentials = {
        nick: event.target["nick"].value.trim(),
      }
      initSocket(credentials.nick)
      store.dispatch(loginRequest(credentials.nick))
    }

  </script>
  <style>
    login{
      display: grid;
      width: 100vw;
      height: 100vh;
      justify-self: center;
      justify-items: center;
      grid-template-columns: 1fr;
      grid-template-rows: min-content auto;
    }
    .fa-comment{
      font-size: 50px;
      grid-row: 1 / 3;
      padding: 1em;
      color: #64C8C8;
    }
    .loginHeader{
      display: grid;
      justify-items: center;
      align-items:center;
      grid-template-columns: 1fr 2fr;
    
    }
    small{
      grid-column: 2 / 3
    }
    .loginForm{
      display: grid;
      grid-template-columns: 1fr;
      min-width: 250px;
      max-height: 50vh;
      align-self: flex-start;
      grid-gap: 1em;
      margin: 1em;
      padding: 1em;
      animation-delay: 500ms;
    }
    .inputLogin{
      font-size: 20px;
      padding: 0.5em;
      border-radius: 25px;
      border: solid 1px black;
      text-align: center
    }
    input {
      padding: 0.5em;
      width: fit-content;
      justify-self: center
    }
    @media only screen and (max-width: 600px) {
      }
  </style>
</login>
