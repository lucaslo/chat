<chat>
  <div class="sidebar animated fadeIn">
    <div class="sidebarItem animated fadeIn {online: user.status==='online'}"  each="{user in this.users}">
      <a>{user.nickname}</a>
    </div>
  </div>
  <div class="chatInterface">
    <div class="messagesContainter" ref='chatWindow'>
      <div class="message animated bounceInDown {systemMessage: message.from==='System' }" each="{message in this.messages}">
        <b class="fromUser">{message.from}</b> <br> <p class="text-message">{message.text}</p>
      </div>
    </div>
    <form class="messageForm" onsubmit="{sendMessage}">
      <input class="inputLogin" type="text" name="message">
      <input type="submit" value="send">
    </form>
  </div>
  <script>
    import {store} from './main.js'
    import {sendMessage} from './state/message/actions'
    import {getSocket} from './socketConnector'
    let socket = getSocket()
    this.messages = store.getState().message
    this.users = store.getState().user.onlineUsers
    store.subscribe(() =>{
      console.log(store.getState())
      this.messages = store.getState().message
      this.users = store.getState().user.onlineUsers
      riot.update()
      this.refs.chatWindow.scrollTop = this.refs.chatWindow.scrollHeight
    })
    this.sendMessage = (event) => {
      event.preventDefault()
      const message = event.target["message"].value.trim()
      event.target["message"].value = ""
      if (socket) {
        socket.emit("message", message)
        store.dispatch(sendMessage(message))
      }
    }

  </script>
  <style>
    chat{
      display: grid;
      position: relative;
      width: 90vw;
      height: 90vh;
      justify-self: center;
      align-self: center;
      justify-items: center;
      grid-template-columns: 20% 80%;
      grid-template-rows: auto;
      overflow-x: hidden;
      overflow-y: hidden;
    }
    .sidebar{
      background-color: #64C8C8;
      height: 100%;
      width: 100%;
      display: grid;
      grid-auto-rows: min-content;
      grid-template-columns: 1fr
    }
    @media(max-width: 800px) {
      chat {
        grid-template-columns: 100%;
        grid-template-rows: min-content 1fr;
        width: 95vw;
        height: 95vh;
      };
      .sidebar{
        background-color: lightblue;
        height: 100%;
        width: 100%;
        display: grid;
        grid-auto-rows: min-content;
        grid-template-columns: 1fr 1fr 1fr;
      };
  }
    .online{
      border-left: solid 10px green;
    }
    .sidebarItem{
      display: grid;
      text-align: start;
      width: auto;
      margin: 0.2em;
      padding: 0.5em;
      background-color: white;
    }
    .messageForm{
      display: grid;
      grid-template-columns: 9fr 1fr;
      align-self: center;
      grid-template-rows: 1fr;
      margin: 0.5em;
    }
    .inputLogin{
      font-size: 18px;
      padding: 1em;
      width: 100%;
    }
    .chatInterface{
      display: grid;
      width: 100%;
      height: 100%;
      grid-template-rows: 85% 15%;
      background-color: #32323E;
      overflow-x: hidden;
      overflow-y: hidden;
    }
    .messagesContainter{
      background-color: #32323E;
      width: auto;
      display: grid;
      max-height: 100%;
      max-height: -moz-available;
      max-height: -webkit-fill-available;
      max-height: fill-available;
      overflow-x: hidden;
      overflow-y: scroll;
      padding: 1em;
      grid-auto-rows: min-content;
      grid-gap: 0.1em;
      margin: 1em;
    }
    .message {
      display: grid;
      word-wrap: break-word;
      overflow-x: hidden;
      padding: 0.2em;
      background-color: white;

    }
    .systemMessage{
      text-align: center;
      color: #E1CE7A;
      background-color: #32323E;
    };
    input {
      padding: 0.2em;
      width: 100%;
      height: 100%;
      font-size: 18px;
    }
    .text-message{
      padding: 0.2em;
      width: auto;
      overflow-x: hidden;
    }

  </style>
</chat>
