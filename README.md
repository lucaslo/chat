# Webpack Chat

Webpack chat, com RiotJs.
É nescessário a execução do socketServer, que também utiliza webpack.
O socketServer pode ser encontrado em https://gitlab.com/lucaslo/socketServer

Design responsivo utilizando css grid.

## Como executar
Com o socketServer já todando.
Primeiro clone esse repositório e navegue para o diretório utilizando uma interface de linha de comando.
É nescessário ter node.js e npm instalados!


```bash
$ npm install
```
```bash
$ npm run start
```

- Abrir [http://localhost:3000/](http://localhost:3000/)
- Entrar com um Nick e utilizar!

Para testar abra diferentes abas. Cada um terá seu socket e será reconhecido como usuario.
O evento disconnect é disparado quando a aba é fechada.

## Tecnologias utilizadas
  RiotJs
  Socket.io
  Redux
